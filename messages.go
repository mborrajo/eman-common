package eman_common

var mod = "Messages"

var MSG_NEW = "NEW"
var MSG_MOD = "MOD"

//var MSG_SAVE = "SAVE"
var MSG_ACK = "ACK"
var MSG_NACK = "NACK"
var MSG_QUERY = "QUERY"
var MSG_QUERYRES = "QUERYRES"

type Message interface {
	GetType() string
	GetEvent() *Event
	GetData() *interface{}
}

type BaseMessage struct {
	MessageType string
	Event       Event
	Data        interface{}
}

func (m BaseMessage) GetData() *interface{} {
	return &m.Data
}

func (m BaseMessage) GetType() string {
	return m.MessageType
}

func (m BaseMessage) GetEvent() *Event {
	return &m.Event
}

/*func (m *Message) Serialize() []byte {
	ret, err := json.Marshal(m)
	CheckError(mod, "Serialize", err)
	return ret
}

func (m *Message) Deserialize(data []byte) {
	err := json.Unmarshal(data, m)
	CheckError(mod, "Deserialize", err)
}*/
