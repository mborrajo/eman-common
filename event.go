package eman_common

import (
	"encoding/json"
	"math"
	"time"
)

type Event struct {
	ID             string
	Key            string `json:"_key"`
	Message        string
	Severity       string
	Status         string
	Host           string
	HostGroup      string
	MonitorGroup   string
	MonitorClass   string
	Instance       string
	Metric         string
	MetricID       string
	MetricValue    float64
	Origin         string
	ReceptionTime  time.Time
	OriginTime     time.Time
	AckTime        time.Time
	ControlledTime time.Time
	EndTime        time.Time
	Propagations   string
	Repetitions    int
	Drop           bool
}

func NewEvent() *Event {
	ret := Event{}
	ret.ReceptionTime = time.Now()
	ret.OriginTime = time.Now()
	return &ret
}

func (ev *Event) ToPrettyString() string {
	res2B, _ := json.MarshalIndent(ev, "", "    ")
	return string(res2B)
}

func (ev *Event) CheckFields() {
	if ev.Severity == "" {
		ev.Severity = "INFO"
	}
	if ev.Status == "" {
		ev.Status = "OPEN"
	}
}

func (ev *Event) ToMap() map[string]interface{} {
	ret := make(map[string]interface{})
	if ev.ID != "" {
		ret["ID"] = ev.ID
	}
	if ev.Message != "undefined" {
		ret["Message"] = ev.Message
	}
	if ev.Status != "undefined" {
		ret["Status"] = ev.Status
	}
	if ev.Severity != "undefined" {
		ret["Severity"] = ev.Severity
	}
	if ev.Host != "undefined" {
		ret["Host"] = ev.Host
	}
	if ev.HostGroup != "undefined" {
		ret["HostGroup"] = ev.HostGroup
	}
	if ev.MonitorGroup != "undefined" {
		ret["MonitorGroup"] = ev.MonitorGroup
	}
	if ev.MonitorClass != "undefined" {
		ret["MonitorClass"] = ev.MonitorClass
	}
	if ev.Instance != "undefined" {
		ret["Instance"] = ev.Instance
	}
	if ev.Metric != "undefined" {
		ret["Metric"] = ev.Metric
	}
	if ev.MetricID != "undefined" {
		ret["MetricID"] = ev.MetricID
	}
	if !math.IsNaN(ev.MetricValue) {
		ret["MetricValue"] = ev.MetricValue
	}
	if ev.Origin != "undefined" {
		ret["Origin"] = ev.Origin
	}
	if ev.Propagations != "undefined" {
		ret["Propagations"] = ev.Propagations
	}
	if !ev.ReceptionTime.IsZero() {
		ret["ReceptionTime"] = ev.ReceptionTime
	}
	if !ev.OriginTime.IsZero() {
		ret["OriginTime"] = ev.OriginTime
	}
	if !ev.AckTime.IsZero() {
		ret["AckTime"] = ev.AckTime
	}
	if !ev.ControlledTime.IsZero() {
		ret["ControlledTime"] = ev.ControlledTime
	}
	if !ev.EndTime.IsZero() {
		ret["EndTime"] = ev.EndTime
	}
	return ret
}
